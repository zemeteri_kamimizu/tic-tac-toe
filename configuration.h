#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <memory>

namespace tic_tac_toe
{

class BoardConfiguration
{
public:
    explicit BoardConfiguration( uint64_t size );
    ~BoardConfiguration();

    uint64_t board_size() const;
private:
    uint64_t size_;
};

class PlayerConfiguration
{
public:
    explicit PlayerConfiguration( uint8_t side, uint8_t type );
    ~PlayerConfiguration();

    uint8_t player_side() const;
    uint8_t player_type() const;

private:
    uint8_t side_;
    uint8_t type_;
};

class IConfiguration
{
public:
    virtual ~IConfiguration() { }

    virtual std::shared_ptr< BoardConfiguration > board() const = 0;
    virtual std::shared_ptr< PlayerConfiguration > playerX() const = 0;
    virtual std::shared_ptr< PlayerConfiguration > player0() const = 0;
};

class CommandLineConfiguration : public IConfiguration
{
public:
    explicit CommandLineConfiguration( int argc, char** argv );
    ~CommandLineConfiguration();

    std::shared_ptr< BoardConfiguration > board() const;
    std::shared_ptr< PlayerConfiguration > playerX() const;
    std::shared_ptr< PlayerConfiguration > player0() const;

private:
    struct Impl;
    std::shared_ptr< Impl > impl_;
};

} // namespace tic_tac_toe

#endif
/* CONFIGURATION_H
*/
