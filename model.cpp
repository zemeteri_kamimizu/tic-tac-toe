#include "model.h"

#include "board.h"
#include "player.h"
#include "playing_side.h"

namespace tic_tac_toe
{

struct Model::Impl
{
    std::shared_ptr< IBoard > board_;
    std::shared_ptr< IPlayer > noughts_;
    std::shared_ptr< IPlayer > crosses_;

    std::shared_ptr< IPlayer > now_turns_;

    bool game_over_;
    char winner_;
};

Model::Model()
{
}

Model::Model( std::shared_ptr< IBoard > & b,
              std::shared_ptr< IPlayer > & p1,
              std::shared_ptr< IPlayer > & p2 )
{
    impl_ = std::make_shared< Model::Impl >();
    impl_->board_ = b;
    impl_->crosses_ = p1;
    impl_->noughts_ = p2;
    impl_->now_turns_ = impl_->crosses_;
    impl_->game_over_ = false;
    impl_->winner_ = 0;
}

Model::~Model()
{
}

std::shared_ptr< IBoard > Model::get_board() const
{
    return impl_->board_;
}

std::shared_ptr< IPlayer > Model::get_current_player() const
{
    return impl_->now_turns_;
}

void Model::set_coords( const std::shared_ptr< board_coordinates > & coords )
{
    impl_->board_->set( coords, impl_->now_turns_->get_side() );

    // Check game is over and possibly the winner
    if ( impl_->board_->full() )
        impl_->game_over_ = true;

    if ( impl_->board_->get_size() == impl_->board_->line( impl_->now_turns_->get_side() ) )
    {
        impl_->winner_ = impl_->now_turns_->get_side()->get();
        impl_->game_over_ = true;
    }

    // Pass turn to another player
    if ( impl_->now_turns_ == impl_->crosses_ )
        impl_->now_turns_ = impl_->noughts_;
    else
        impl_->now_turns_ = impl_->crosses_;

    // Notify observers:
    notify( const_cast< const Model & >( *this ) );
}

bool Model::game_over() const
{
    return impl_->game_over_;
}

char Model::winner() const
{
    return impl_->winner_;
}

void Model::start()
{
    notify( const_cast< const Model & >( *this ) );
}

} // namespace tic_tac_toe
