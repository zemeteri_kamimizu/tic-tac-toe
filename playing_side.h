#ifndef PLAYING_SIDE_H
#define PLAYING_SIDE_H

namespace tic_tac_toe
{

class IPlayingSide
{
public:
    virtual ~IPlayingSide() { }

    virtual char get() const = 0;
};

class Noughts : public IPlayingSide
{
public:
    Noughts();
    ~Noughts();

    char get() const;
};

class Crosses : public IPlayingSide
{
public:
    Crosses();
    ~Crosses();

    char get() const;
};

} // namespace tic_tac_toe

#endif
/* PLAYING_SIDE_H
*/
