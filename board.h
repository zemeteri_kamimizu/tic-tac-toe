#ifndef BOARD_H
#define BOARD_H

#include <memory>

namespace tic_tac_toe
{

class IPlayingSide;

class board_coordinates
{
public:
    board_coordinates( int, int );

    int x_;
    int y_;
};

class IBoard
{
public:
    virtual ~IBoard() { }

    virtual int get_size() const = 0;
    virtual bool empty( const std::shared_ptr< board_coordinates > & ) const = 0;
    virtual int line( const std::shared_ptr< IPlayingSide > & ) const = 0;
    virtual bool full() const = 0;
    virtual char value( const std::shared_ptr< board_coordinates > & ) const = 0;

    virtual void set( const std::shared_ptr< board_coordinates > &, const std::shared_ptr< IPlayingSide > & ) = 0; 
};

class ConcreteBoard : public IBoard
{
public:
    ConcreteBoard( int size = 3 );
    ~ConcreteBoard();

    int get_size() const;
    bool empty( const std::shared_ptr< board_coordinates > & ) const;
    int line( const std::shared_ptr< IPlayingSide > & ) const;
    bool full() const;
    char value( const std::shared_ptr< board_coordinates > & ) const;

    void set( const std::shared_ptr< board_coordinates > &, const std::shared_ptr< IPlayingSide > & );

private:
    struct Impl;
    std::shared_ptr< Impl > impl_;
};

} // namespace tic_tac_toe

#endif
/* BOARD_H
*/
