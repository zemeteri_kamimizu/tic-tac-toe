#ifndef STUPID_MATRIX_H
#define STUPID_MATRIX_H

#include <vector>
#include <stdexcept>

template< typename T >
class matrix
{
public:
    explicit matrix( int m, int n ) :
        v_( m * n ),
        m_( m ),
        n_( n )
    {
    }

    ~matrix()
    {
    }

    const T& get( int i, int j ) const
    {
        if ( i >= n_ || j >= m_ )
            throw std::invalid_argument( std::string( "One or both arguments are out of matrix bounds" ) );
        return v_[ i * n_ + j ];
    }

    void set( int i, int j, const T & val )
    {
        if ( i >= n_ || j >= m_ )
            throw std::invalid_argument( std::string( "One or both arguments are out of matrix bounds" ) );
        v_[ i * n_ + j ] = val;
    }

private:
    int m_;
    int n_;
    std::vector< T > v_;
};

#endif
/* STUPID_MATRIX_H
*/
