#include "player.h"

#include "board.h"
#include "playing_side.h"
#include "input.h" 

namespace tic_tac_toe
{

struct Player::Impl
{
    explicit Impl( const std::shared_ptr< IPlayingSide > & ps, std::shared_ptr< IInput > & inp ) :
        side_( ps ),
        input_( inp )
    {
    }
    std::shared_ptr< IPlayingSide > side_;
    std::shared_ptr< IInput > input_;
};

Player::Player( const std::shared_ptr< IPlayingSide > & ps, std::shared_ptr< IInput > & inp )
{
    impl_ = std::make_shared< Player::Impl >( ps, inp );
}

Player::~Player()
{
}

std::shared_ptr< IPlayingSide > Player::get_side() const
{
    return impl_->side_;
}

std::shared_ptr< board_coordinates > Player::make_turn()
{
    return impl_->input_->get();
}

} // namespace tic_tac_toe
