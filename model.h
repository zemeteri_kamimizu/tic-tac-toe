#ifndef MODEL_H
#define MODEL_H

#include "observer.h"

#include <memory>

namespace tic_tac_toe
{

class board_coordinates;
class IBoard;
class IPlayer;

class Model : public AbstractObservable
{
public:
    explicit Model( std::shared_ptr< IBoard > &,
                    std::shared_ptr< IPlayer > &,
                    std::shared_ptr< IPlayer > & );
    ~Model();

    std::shared_ptr< IBoard > get_board() const;
    std::shared_ptr< IPlayer > get_current_player() const;
    void set_coords( const std::shared_ptr< board_coordinates > & );

    bool game_over() const;
    char winner() const;

    void start();
private:
    struct Impl;
    std::shared_ptr< Impl > impl_;

private:
    Model();
};

} // namespace tic_tac_toe

#endif
/* MODEL_H
*/
