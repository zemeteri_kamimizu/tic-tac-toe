#include "board.h"

#include "playing_side.h"
#include "stupid_matrix.h"

namespace tic_tac_toe
{

board_coordinates::board_coordinates( int x, int y ) :
    x_( x ),
    y_( y )
{
}

struct ConcreteBoard::Impl
{
    Impl( int size ) :
        m_( size, size ),
        size_( size ),
        count_( 0 )
    {
    }

    int count_;
    int size_;
    matrix< char > m_;

    bool find( const char c, int & i, int & j )
    {
        for ( i = 0; i < size_; ++i )
        {
            for ( j = 0; j < size_; ++j )
            {
                if ( m_.get( i, j ) == c )
                    return true;
            }
        }
        return false;
    }

    int count_col( const char c, int j )
    {
        int i = 0, count = 0;
        for ( ; i < size_; ++i )
            if ( m_.get( i, j ) == c )
                ++count;
        return count;
    }

    int count_row( const char c, int i )
    {
        int j = 0, count = 0;
        for ( ; j < size_; ++j )
            if ( m_.get( i, j ) == c )
                ++count;
        return count;
    }

    int count_dia( const char c )
    {
        int i = 0, j = 0, count = 0;
        for ( ; i < size_, j < size_; ++i, ++j )
            if ( m_.get( i, j ) == c )
                ++count;

        if ( count == size_ )
            return count;

        count = 0;
        for ( i = 0, j = size_ - 1; i < size_, j >= 0; ++i, --j )
            if ( m_.get( i, j ) == c )
                ++count;
        return count;
    }
};

ConcreteBoard::ConcreteBoard( int size )
{
    impl_ = std::make_shared< ConcreteBoard::Impl >( size );
}

ConcreteBoard::~ConcreteBoard()
{
}

int ConcreteBoard::get_size() const
{
    return impl_->size_;
}

bool ConcreteBoard::empty( const std::shared_ptr< board_coordinates > & coords ) const
{
    return !impl_->m_.get( coords->x_, coords->y_ ) ? true : false ;
}

int ConcreteBoard::line( const std::shared_ptr< IPlayingSide > & ps ) const
{
    int i = 0, j = 0;
    int line = 0, col = 0, dia = 0;
    if ( !impl_->find( ps->get(), i, j ) )
        return 0;

    if ( i == 0 )
    {
        for ( int j_ = 0; j_ < impl_->size_; ++j_ )
        {
            int l = impl_->count_col( ps->get(), j_ );
            if ( l > col )
                col = l; 
        }
    }
    if ( j == 0 )
    {
        for ( int i_ = 0; i_ < impl_->size_; ++i_ )
        {
            int l = impl_->count_row( ps->get(), i );
            if ( l > line )
                line = l;
        }
    }

    dia = impl_->count_dia( ps->get() );

    return line > col ? ( line > dia ? line : dia ) : ( col > dia ? col : dia );
}

bool ConcreteBoard::full() const
{
    return impl_->count_ < ( impl_->size_ * impl_->size_ ) ? false : true;
}

char ConcreteBoard::value( const std::shared_ptr< board_coordinates > & coords ) const
{
    return impl_->m_.get( coords->x_, coords->y_ );
}

void ConcreteBoard::set( const std::shared_ptr< board_coordinates > & coords, const std::shared_ptr< IPlayingSide > & ps )
{
    if ( !empty( coords ) )
        throw std::invalid_argument( "Can not mark this place" );
    impl_->m_.set( coords->x_, coords->y_, ps->get() );
    ++( impl_->count_ );
}

} // namespace tic_tac_toe
