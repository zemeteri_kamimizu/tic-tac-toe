#include "controller.h"

#include "model.h"
#include "player.h" 

namespace tic_tac_toe
{

Controller::Controller( std::shared_ptr< Model > & model ) :
    model_( model )
{
}

Controller::~Controller()
{
}

void Controller::get_input()
{
    std::shared_ptr< IPlayer > p( model_->get_current_player() );
    model_->set_coords( p->make_turn() );
}

} // namespace tic_tac_toe
