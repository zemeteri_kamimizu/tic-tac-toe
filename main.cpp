#include "configuration.h"

#include "input.h"
#include "board.h"

#include "playing_side.h"
#include "player.h"

#include "view.h"
#include "model.h"
#include "controller.h"

#include <iostream>
#include <memory>

int main( int argc, char** argv )
{
    std::shared_ptr< tic_tac_toe::IConfiguration > config;
    try
    {
        config = std::make_shared< tic_tac_toe::CommandLineConfiguration >( argc, argv );
    }
    catch ( const std::invalid_argument & e )
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
    catch ( const std::out_of_range & e )
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
    catch ( const std::logic_error & e )
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
    catch ( ... )
    {
        std::cout << "Exception of unexpected type cought - too bad" << std::endl;
        return 1;
    }

    std::shared_ptr< tic_tac_toe::IBoard > board = std::make_shared< tic_tac_toe::ConcreteBoard >( config->board()->board_size() );
    std::shared_ptr< tic_tac_toe::IInput > con_input = std::make_shared< tic_tac_toe::ConsoleInput >();
    std::shared_ptr< tic_tac_toe::IInput > rnd_input = std::make_shared< tic_tac_toe::RandomInput >( board->get_size() );

    std::shared_ptr< tic_tac_toe::IPlayingSide > noughts = std::make_shared< tic_tac_toe::Noughts >();
    std::shared_ptr< tic_tac_toe::IPlayingSide > crosses = std::make_shared< tic_tac_toe::Crosses >();

    std::shared_ptr< tic_tac_toe::IInput > inp;
    if ( config->playerX()->player_type() == 'h' )
        inp = con_input;
    else
        inp = rnd_input;
    std::shared_ptr< tic_tac_toe::IPlayer > player1 = std::make_shared< tic_tac_toe::Player >( crosses, inp );

    if ( config->player0()->player_type() == 'h' )
        inp = con_input;
    else
        inp = rnd_input;
    std::shared_ptr< tic_tac_toe::IPlayer > player2 = std::make_shared< tic_tac_toe::Player >( noughts, inp );
    // std::shared_ptr< tic_tac_toe::IPlayer > player2 = std::make_shared< tic_tac_toe::Player >( noughts, con_input );

    std::shared_ptr< tic_tac_toe::Model > model = std::make_shared< tic_tac_toe::Model >( board, player1, player2 );
    std::shared_ptr< IObserver > view = std::make_shared< tic_tac_toe::ConsoleView >();
    model->add_observer( view );

    tic_tac_toe::Controller controller( model );

    model->start();

    while ( !model->game_over() )
    {
        try
        {
            controller.get_input();
        }
        catch ( ... )
        {
            std::cout << "Oops" << std::endl;
        }
    }

    return 0;
}
