#ifndef PLAYER_H
#define PLAYER_H

#include <memory>

namespace tic_tac_toe
{

class board_coordinates;
class IPlayingSide;
class IInput;

class IPlayer
{
public:
    virtual ~IPlayer() { }

    virtual std::shared_ptr< IPlayingSide > get_side() const = 0;
    virtual std::shared_ptr< board_coordinates > make_turn() = 0;
};

class Player : public IPlayer
{
public:
    explicit Player( const std::shared_ptr< IPlayingSide > &, std::shared_ptr< IInput > & );
    ~Player();

    std::shared_ptr< IPlayingSide > get_side() const;
    std::shared_ptr< board_coordinates > make_turn();

private:
    struct Impl;
    std::shared_ptr< Impl > impl_;
};

} // namespace tic_tac_toe 

#endif
/* PLAYER_H
*/
