#ifndef VIEW_H
#define VIEW_H

#include "observer.h"

namespace tic_tac_toe
{

class Model;

class ConsoleView : public IObserver
{
public:
    ConsoleView();
    ~ConsoleView();

    void Update( const Model & );
};

} // namespace tic_tac_toe

#endif
/* VIEW_H
*/
