#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <memory>

namespace tic_tac_toe
{

class Model;

class Controller
{
public:
    explicit Controller( std::shared_ptr< Model > & );
    ~Controller();

    void get_input();

private:
    std::shared_ptr< Model > model_;
};

} // namespace tic_tac_toe

#endif
/* CONTROLLER_H
*/
