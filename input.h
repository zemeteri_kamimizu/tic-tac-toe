#ifndef INPUT_H
#define INPUT_H

#include <memory>
#include <random>

namespace tic_tac_toe
{

class board_coordinates;

class IInput
{
public:
    virtual ~IInput() { }

    // data in form of formatted string
    virtual std::shared_ptr< board_coordinates > get() = 0;
};

class ConsoleInput : public IInput
{
public:
    ConsoleInput();
    ~ConsoleInput();

    std::shared_ptr< board_coordinates > get();
};

class RandomInput : public IInput
{
public:
    RandomInput( uint64_t );
    ~RandomInput();

    std::shared_ptr< board_coordinates > get();
private:
    std::default_random_engine gen_;
    std::uniform_int_distribution< uint64_t > dist_;
};

} // namespace tic_tac_toe

#endif
/* INPUT_H
*/
