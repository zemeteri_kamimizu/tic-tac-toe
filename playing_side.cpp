#include "playing_side.h"

namespace tic_tac_toe
{

Noughts::Noughts()
{
}

Noughts::~Noughts()
{
}

char Noughts::get() const
{
    return '0';
}

Crosses::Crosses()
{
}

Crosses::~Crosses()
{
}

char Crosses::get() const
{
    return 'X';
}

} // namespace tic_tac_toe
