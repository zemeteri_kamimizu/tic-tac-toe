#include "configuration.h"

namespace tic_tac_toe
{

const uint64_t min_board_size = 3;
const uint64_t max_board_size = 9;
const uint64_t default_board_size = min_board_size; 

BoardConfiguration::BoardConfiguration( uint64_t size ) :
    size_( size )
{
    if ( ( size_ < min_board_size ) || ( size_ > max_board_size ) )
        throw std::out_of_range( "Game field size out of range: 3 <= size <= 9" );
}

BoardConfiguration::~BoardConfiguration()
{
}

uint64_t BoardConfiguration::board_size() const
{
    return size_;
}

PlayerConfiguration::PlayerConfiguration( uint8_t side, uint8_t type ) :
        side_( side ),
        type_( type )
{
    if ( type != 'h' && type != 'c' )
        throw std::invalid_argument( "Invalid player type, must be 'h' or 'c'" );
    if ( side != 'X' && side != '0' )
        throw std::invalid_argument( "Invalid player side, must be 'X' or '0'" );
}

PlayerConfiguration::~PlayerConfiguration()
{
}

uint8_t PlayerConfiguration::player_side() const
{
    return side_;
}

uint8_t PlayerConfiguration::player_type() const
{
    return type_;
}

struct CommandLineConfiguration::Impl
{
    int argc_;
    char** argv_;
    std::shared_ptr< BoardConfiguration > board_;
    std::shared_ptr< PlayerConfiguration > playerX_;
    std::shared_ptr< PlayerConfiguration > player0_;
};

/* arguments:
  -b size
  -x 'h' | 'c'
  -0 'h' | 'c'
*/
CommandLineConfiguration::CommandLineConfiguration( int argc, char** argv )
{
    impl_ = std::make_shared< CommandLineConfiguration::Impl >();
    impl_->argc_ = argc;
    impl_->argv_ = argv;

    for ( int i = 0; i < argc; ++i )
    {
        std::string k( argv[ i ] );
        if ( k == "-x" ) // set player of 'X' side
            impl_->playerX_ = std::make_shared< PlayerConfiguration >( 'X', argv[ i + 1 ][ 0 ] );
        if ( k == "-0" ) // set player of '0' side
            impl_->player0_ = std::make_shared< PlayerConfiguration >( '0', argv[ i + 1 ][ 0 ] );
        if ( k == "-b" )
        {
            try
            {
                impl_->board_ = std::make_shared< BoardConfiguration >( std::stoi( std::string( argv[ i + 1 ] ), nullptr ) );
            }
            catch ( const std::invalid_argument & e )
            {
                throw std::invalid_argument( std::string( "Board size value invalid: " ) + e.what() );
            }
            catch ( const std::out_of_range & e )
            {
                throw std::out_of_range( std::string( "Board size value is out of range: " ) + e.what() );
            }
        }
    }

    if ( !impl_->board_ )
        impl_->board_ = std::make_shared< BoardConfiguration >( default_board_size );

    if ( !impl_->playerX_ )
        impl_->playerX_ = std::make_shared< PlayerConfiguration >( 'X', 'h' );

    if ( !impl_->player0_ )
        impl_->player0_ = std::make_shared< PlayerConfiguration >( '0', 'c' );

    if ( !impl_->playerX_ || !impl_->player0_ )
        throw std::logic_error( "One or more parameters are absent or invalid" );
}

CommandLineConfiguration::~CommandLineConfiguration()
{
}

std::shared_ptr< BoardConfiguration > CommandLineConfiguration::board() const
{
    return impl_->board_;
}

std::shared_ptr< PlayerConfiguration > CommandLineConfiguration::playerX() const
{
    return impl_->playerX_;
}

std::shared_ptr< PlayerConfiguration > CommandLineConfiguration::player0() const
{
    return impl_->player0_;
}

} // namespace tic_tac_toe
