#include "view.h"

#include "model.h"
#include "board.h"
#include "player.h"
#include "playing_side.h"

#include "iostream"

namespace tic_tac_toe
{

ConsoleView::ConsoleView()
{
}

ConsoleView::~ConsoleView()
{
}

void ConsoleView::Update( const Model & model )
{
    int board_size = model.get_board()->get_size();
    std::string s;

    std::cout << std::endl << "=========" << std::endl;
    for ( int i = 0; i < board_size; ++i )
    {
        for ( int j = 0; j < board_size; ++j )
        {
            char c = model.get_board()->value( std::make_shared< board_coordinates >( i, j ) );
            if ( !c )
                c = ' ';
            s += c;
        }
        std::cout << s << std::endl;
        s.clear();
    }

    // Check if game is over
    if ( model.game_over() )
    {
        std::cout << "Game over!" << std::endl;
        if ( model.winner() )
        {
            std::cout << "Player '" << model.winner() << "' wins the game!" << std::endl;
        }
        return;
    }

    s += std::string("Player[") + model.get_current_player()->get_side()->get() + std::string( "](x,y)=>" );
    std::cout << s ;
}

} // namespace tic_tac_toe
