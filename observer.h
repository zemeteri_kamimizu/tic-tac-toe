#ifndef OBSERVER_H
#define OBSERVER_H

#include <memory>

class IObserver;

namespace tic_tac_toe
{
class Model;
} // namespace tic_tac_toe

class IObservable
{
public:
    virtual ~IObservable() { }

    virtual void add_observer( std::shared_ptr< IObserver > & ) = 0;
    virtual void del_observer( std::shared_ptr< IObserver > & ) = 0;

    virtual void notify( const tic_tac_toe::Model & ) = 0;
};

class IObserver
{
public:
    virtual ~IObserver() { }

    virtual void Update( const tic_tac_toe::Model & ) = 0;
};

// Higher abstraction level

class AbstractObservable : public IObservable
{
public:
    AbstractObservable();
    ~AbstractObservable();

    void add_observer( std::shared_ptr< IObserver > & );
    void del_observer( std::shared_ptr< IObserver > & );

    void notify( const tic_tac_toe::Model & );

private:
    struct Impl;
    std::shared_ptr< Impl > impl_;
};

#endif
/* OBSERVER_H
*/
