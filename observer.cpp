#include "observer.h"

#include "model.h"

#include <list>

struct AbstractObservable::Impl
{
    std::list< std::shared_ptr< IObserver > > observers_;
};

AbstractObservable::AbstractObservable()
{
    impl_ = std::make_shared< AbstractObservable::Impl >();
}

AbstractObservable::~AbstractObservable()
{
}

void AbstractObservable::add_observer( std::shared_ptr< IObserver > & observer )
{
    impl_->observers_.push_back( observer );
}

void AbstractObservable::del_observer( std::shared_ptr< IObserver > & observer )
{
    std::list< std::shared_ptr< IObserver > >::iterator it = impl_->observers_.begin();
    std::list< std::shared_ptr< IObserver > >::iterator end = impl_->observers_.end();
    for( ; it != end; ++it )
        if ( (*it) == observer )
            impl_->observers_.erase( it );
}

void AbstractObservable::notify( const tic_tac_toe::Model & m )
{
    std::list< std::shared_ptr< IObserver > >::iterator it = impl_->observers_.begin();
    std::list< std::shared_ptr< IObserver > >::iterator end = impl_->observers_.end();
    for( ; it != end; ++it )
        (*it)->Update( m );
}