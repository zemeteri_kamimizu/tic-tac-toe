#include "input.h"

#include "board.h"

// for ConsoleInput
#include <string>
#include <iostream>

// for RandomInput
// #include <random>

namespace tic_tac_toe
{

ConsoleInput::ConsoleInput()
{
}

ConsoleInput::~ConsoleInput()
{
}

std::shared_ptr< board_coordinates > ConsoleInput::get()
{
    std::string inp_str;
 
    std::size_t delim = 0;

    int x = 0, y = 0;

    try
    {
        std::cin >> inp_str;
        x = std::stoi( inp_str, &delim );
        std::cin >> inp_str;
        y = std::stoi( inp_str, &delim );
    }
    catch ( const std::invalid_argument & e )
    {
        throw; // think what to do
    }
    catch ( const std::out_of_range & e )
    {
        throw; // think what to do
    }
    catch ( ... )
    {
        return std::shared_ptr< board_coordinates >();
    }

    return std::make_shared< board_coordinates >( x, y );
}

RandomInput::RandomInput( uint64_t board_size) :
    dist_( 0, board_size - 1 )
{
}

RandomInput::~RandomInput()
{
}

std::shared_ptr< board_coordinates > RandomInput::get()
{
    uint64_t i = dist_( gen_ );
    uint64_t j = dist_( gen_ );
    return std::make_shared< board_coordinates >( i, j );
}

} // namespace tic_tac_toe
